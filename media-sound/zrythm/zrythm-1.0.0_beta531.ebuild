# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
#IUSE="jack boost simd scripts lv2 backrace sdl vst"
DESCRIPTION="Высокоавтоматизированная и интуитивно понятная цифровая аудио рабочая станция"
HOMEPAGE="https://www.zrythm.org/"
SRC_URI="https://www.zrythm.org/releases/zrythm-1.0.0-beta.5.3.1.tar.xz -> ${P}.tar.xz"
LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64"
 src_unpack() {
		unpack ${P}.tar.xz
	#	use foo && unpack ${P}.tar.bz2
	mv  ${WORKDIR}/zrythm-1.0.0-beta.5.3.1  ${WORKDIR}/${P} || die
}



DEPEND="
	dev-util/meson
	dev-scheme/guile
	dev-lang/sassc
"
RDEPEND="${DEPEND}
	kde-frameworks/breeze-icons
	media-sound/carla
	sci-libs/fftw
	gui-libs/gtk
	gui-libs/gtksourceview
	gui-libs/libadwaita
	net-misc/curl
	dev-libs/libyaml
	gui-libs/libpanel
	media-libs/libsndfile
	media-libs/lilv
	dev-libs/libpcre2
	sci-libs/keras-preprocessing
	media-libs/rubberband
	media-libs/soxr
	media-libs/vamp-plugin-sdk
	dev-libs/xxhash
	dev-libs/yyjson
	dev-libs/zix
	app-arch/zstd


"
BDEPEND=""
