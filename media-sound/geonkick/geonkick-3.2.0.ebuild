# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
IUSE="jack VST3"
DESCRIPTION="Генериротор широкого спектра перкуссионных звуков"
HOMEPAGE="https://geonkick.org"
SRC_URI="https://gitlab.com/Geonkick-Synthesizer/geonkick/-/archive/v3.2.0/geonkick-v3.2.0.tar.bz2 -> ${P}.tar.bz2"
LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64"
src_unpack() {
		unpack ${P}.tar.bz2
	#	use foo && unpack ${P}.tar.bz2
	mv  ${WORKDIR}/geonkick-v3.2.0  ${WORKDIR}/${P} || die
}
DEPEND=""
RDEPEND="${DEPEND}
	media-libs/libsndfile
	dev-libs/openssl
	dev-libs/rapidjson
	#media-sound/jack-audio-connection-kit
	media-libs/lv2"

BDEPEND="
	jack? media-sound/jack-audio-connection-kit
	

"
